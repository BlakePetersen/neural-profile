#!/usr/bin/env python
import constants as c
import subprocess
import threading
import tweepy

CONSUMER_KEY = c.ACCT_DATA['twitter']['consumer']['key']
CONSUMER_SECRET = c.ACCT_DATA['twitter']['consumer']['secret']

ACCESS_TOKEN = '995546167-EXPBkOIjXrZJT8vM47gLYVc8HJuwAh5ZbtmTJDyn'
ACCESS_TOKEN_SECRET = 'muK1D5QfBXE63izPL08VFJvv8ZTNmO98PyQgxZyPg'

CONTENT_IMG = c.IMG_DATA['content']
STYLE_IMG = c.IMG_DATA['style']

print CONTENT_IMG
print STYLE_IMG

torch_args = [
  'th', 'neural_style.lua',
  '-style_image', STYLE_IMG,
  '-content_image', CONTENT_IMG,
  '-gpu', '-1',
  '-num_iterations', c.ITERS,
  '-image_size', c.SIZE,
  #'-tv_weight', '1e-2'
]

auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)

api = tweepy.API(auth)

def updateProfileImagery(prof_img, bg_img):
  #api.update_profile_image(prof_img)
  #api.update_profile_background_image(bg_img)
  return


def popenAndCall(onExit, *popenArgs, **popenKWArgs):
    """
    Runs a subprocess.Popen, and then calls the function onExit when the
    subprocess completes.

    Use it exactly the way you'd normally use subprocess.Popen, except include a
    callable to execute as the first argument. onExit is a callable object, and
    *popenArgs and **popenKWArgs are simply passed up to subprocess.Popen.
    """
    def runInThread(onExit, popenArgs, popenKWArgs):
        subprocess.call(*popenArgs, **popenKWArgs)
        onExit()
        return

    thread = threading.Thread(target=runInThread,
                              args=(onExit, popenArgs, popenKWArgs))
    thread.start()

    return thread # returns immediately after the thread starts


popenAndCall(updateProfileImagery('out.png', STYLE_IMG), torch_args)
